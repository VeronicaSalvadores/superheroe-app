# SuperHeroe-App

Proyecto realizado por Verónica Salvadores para asentar conocimientos de Angular atacando a dos APIS: Api pública de SuperHeroeApi https://superheroapi.com y una back-api de creación propia sobre creación de Super Heroes a través de un formulario.

Trabajamos con typescript, creando comunicación entre componentes, rutas, directivas y template, formularios reactivos, servicios y observables. Implementación de lazy-loading. Pondremos en práctica también todos los conocimientos adquiridos en HTML, CSS, SCSS, metodología BEM y JS.

### Instalación / Arranque Proyecto:

**Levantar el back-api**

Sitúate con la terminal en la carpeta back-api.
Ejecutar:
npm i json-server (la primera vez que lo abrimos)
npm run server


**Levantar el proyecto**

Sitúate en la carpeta del proyecto
Ejecuta npm i (solo la primera vez)
Situate en la carpeta /src/app
Ejecuta ng serve

Navega a `http://localhost:4200/`. La app se cargará automáticamente si haces algún cambio.

### Proyecto-Angular

# Creación de una fake api con la interfaz que vamos a utilizar

# Creamos:

1. Componentes comunes: Footer y Header
2. Pages: About, Details, Finder, Form y Home, cada uno con sus correspondientes componentes hijos
3. Dos servicios, uno para atacar a la Api web y otro para atacar a la Fake Api
4. Carpeta Models + interfaces

# AngularLazyLoading

Hemos trabajado con LazyLoading, de esta manera la carga de archivos pesará menos en la app.
Este proyecto ha sido generado con [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.
