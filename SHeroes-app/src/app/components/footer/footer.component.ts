import { Component, OnInit } from '@angular/core';
import { Footer } from 'src/app/models/Interface-footer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public importLogo: string = '../../../assets/img/Logo.png';
  public altLogo: string = 'SuperHeroes Logo';

  footer: Footer | any  = {};
  information: string[];
  help: string[];
  community: string[];

  constructor() {

    this.information = ['About', 'Blog', 'Site'];
    this.help = ['Help', 'Q&A', 'Community'];
    this.community = ['Instragram', 'Twitter', 'Facebook'];

  }

    ngOnInit(): void {

    this.footer = {
      information: this.information,
      help: this.help,
      community: this.community,
    };
  }
}
