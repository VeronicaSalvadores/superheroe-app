import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public importLogo: string = '../../../assets/img/Logo.png';
  public altLogo: string = 'SuperHeroes Logo';

  constructor() { }

  ngOnInit(): void {
  }

}
