export interface Iabout{
    title: string;
    subtitle: string;
    link: string;
    image: string;
    description: string;
}
