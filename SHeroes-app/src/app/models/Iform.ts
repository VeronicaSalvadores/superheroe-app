export interface IformHeroes{
    namesuperheroe: string;
    fullname: string;
    gender: 'Female' | 'Male' | 'Unknown';
    origin: string;
    intelligence: number;
    strength: number;
    speed: number;
    durability: number;
    power: string;
    combat: string;
    image: string;
}
