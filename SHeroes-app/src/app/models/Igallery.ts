export interface IGallery{
    imagesGallery: Image;
}

export interface Image{
    url: string;
}
