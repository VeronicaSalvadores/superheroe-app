export interface Footer {
  information: string[];
  help: string[];
  community: string[];
}
