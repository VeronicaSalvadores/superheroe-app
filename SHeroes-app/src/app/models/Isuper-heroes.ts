export interface IsuperHeroesGeneral {
    id: string;
    name: string;
    image: string;
}

export interface IsuperHeroesDetails {
    id: string;
    image: string;
    name: string;
    powerstats: Powerstats;
    biography: Biography;
    appearance: Appeareance;
}

export interface Powerstats {
    intelligence: string;
    strength: string;
    speed: string;
    durability: string;
    power: string;
    combat: string;
}

export interface Biography {
    fullname: string;
}

export interface Appeareance {
    gender: string;
    race: string;
}
