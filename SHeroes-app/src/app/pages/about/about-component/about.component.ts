import { Component, OnInit } from '@angular/core';
import { Iabout } from '../../../models/Iabout';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  about: Iabout | any  = {};
  title: string;
  subtitle: string;
  description: string;
/*   link: string; */
  image: string;

  constructor() {

    this.title = 'SuperHeroe-App';
    this.subtitle = '- Verónica Salvadores -';
    this.description = 'SuperHeroApp es una web con la que podrás consultar una base de datos de SuperHeroes y detalles sobre sus PowerStats además de poder crear tus propios SuperHeroes añadiendo datos personalizados.';
/*     this.link = 'GitLab'; */
    this.image = 'https://androidayuda.com/app/uploads-androidayuda.com/2020/06/apps-fondos-pantalla-superheroes.jpg';
  }

  ngOnInit(): void {

    this.about = {
      title: this.title,
      subtitle: this.subtitle,
      description: this.description,
/*       link: this.link, */
      image: this.image,
    };
  }

}
