import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SHeroesService } from './../../../services/sheroes.service';
import { IsuperHeroesDetails } from '../../../models/Isuper-heroes';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  public detailsList: IsuperHeroesDetails | any = [];
  private superheroesId: string | any;

  // tslint:disable-next-line: no-shadowed-variable
  constructor(private superHeroeService: SHeroesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSuperHeroesDetails();
  }

  public getSuperHeroesDetails(): void{
    this.route.paramMap.subscribe(params => {
    this.superheroesId = params.get('id');
    });
    this.superHeroeService.getSuperHeroesDetails(Number(this.superheroesId)).subscribe((data: IsuperHeroesDetails) => {
      this.detailsList = data;
    }, (err) =>
      {console.error(err.message);
    });
  }
}
