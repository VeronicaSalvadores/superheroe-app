import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

import { IsuperHeroesDetails } from '../../../../models/Isuper-heroes';
import { SHeroesService } from './../../../../services/sheroes.service';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})

export class ItemDetailsComponent implements OnInit {

  @Input() details: IsuperHeroesDetails | any = [];


  // tslint:disable-next-line: no-shadowed-variable
  constructor(private location: Location, private superHeroeService: SHeroesService) { }

  ngOnInit(): void {
  }

  public goBack(): void {
    this.location.back();
  }
}
