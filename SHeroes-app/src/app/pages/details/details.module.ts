import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsRoutingModule } from './details-routing.module';
import { DetailsComponent } from './details-component/details.component';
import { ItemDetailsComponent } from './details-component/item-details/item-details.component';

@NgModule({
  declarations: [DetailsComponent, ItemDetailsComponent],
  imports: [
    CommonModule,
    DetailsRoutingModule
  ]
})
export class DetailsModule { }
