import { Component, OnInit } from '@angular/core';

import { SHeroesService } from './../../../services/sheroes.service';
import { IsuperHeroesGeneral } from '../../../models/Isuper-heroes';


@Component({
  selector: 'app-finder',
  templateUrl: './finder.component.html',
  styleUrls: ['./finder.component.scss']
})
export class FinderComponent implements OnInit {

  public superheroesList: IsuperHeroesGeneral | any = [];

  constructor(private superHeroeService: SHeroesService) { }

  ngOnInit(): void {
    this.getSuperHeroes();
  }

  getSuperHeroes(): void{
    for (let i = 1; i < 13; i++) {
      this.superHeroeService.getSuperHeroes(i).subscribe(
      (data: IsuperHeroesGeneral) => {
      this.superheroesList.push(data);
      },
      (err) => {
        console.error(err.message);
      });
    }
  }
}
