import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSuperheroesComponent } from './item-superheroes.component';

describe('ItemSuperheroesComponent', () => {
  let component: ItemSuperheroesComponent;
  let fixture: ComponentFixture<ItemSuperheroesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemSuperheroesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSuperheroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
