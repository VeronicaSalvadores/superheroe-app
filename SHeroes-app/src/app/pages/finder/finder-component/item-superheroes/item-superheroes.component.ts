import { Component, Input, OnInit } from '@angular/core';

import { IsuperHeroesGeneral } from 'src/app/models/Isuper-heroes';
import { SHeroesService } from './../../../../services/sheroes.service';


@Component({
  selector: 'app-item-superheroes',
  templateUrl: './item-superheroes.component.html',
  styleUrls: ['./item-superheroes.component.scss']
})
export class ItemSuperheroesComponent implements OnInit {

  @Input() superheroes: IsuperHeroesGeneral | any = [];

  constructor(private superHeroeService: SHeroesService) { }

  ngOnInit(): void {
  }

}
