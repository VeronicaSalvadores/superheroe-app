import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinderRoutingModule } from './finder-routing.module';
import { FinderComponent } from './finder-component/finder.component';
import { ItemSuperheroesComponent } from './finder-component/item-superheroes/item-superheroes.component';


@NgModule({
  declarations: [FinderComponent, ItemSuperheroesComponent],
  imports: [
    CommonModule,
    FinderRoutingModule
  ]
})
export class FinderModule { }
