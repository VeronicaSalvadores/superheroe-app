import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { of } from 'rxjs';
import { IformHeroes } from '../../../models/Iform';
import { ServipostService } from '../../../services/servipost.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {

  public formHeroes: FormGroup | any = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;
  gender: [] | any;
  public superFakeList: IformHeroes[] | any;

  constructor(private formBuilder: FormBuilder, private servipostService: ServipostService) {}

  public createSuperHeroeForm(): void {

    this.formHeroes = this.formBuilder.group({
      namesuperheroe: ['', [Validators.required, Validators.minLength(2)]],
      fullname: ['', [Validators.required, Validators.minLength(2)]],
      gender: [''],
      origin: ['', [Validators.required, Validators.minLength(2)]],
      intelligence: ['', [Validators.required, Validators.maxLength(3)]],
      strength: ['', [Validators.required, Validators.maxLength(3)]],
      speed: ['', [Validators.required, Validators.maxLength(3)]],
      durability: ['', [Validators.required, Validators.maxLength(3)]],
      power: ['', [Validators.required, Validators.maxLength(3)]],
      combat: ['', [Validators.required, Validators.maxLength(3)]],
      image: ['', [Validators.required, Validators.minLength(10)]],
    });

    of(this.getGenders()).subscribe(gender => {
      this.gender = gender;
      this.formHeroes.controls.gender.patchValue(this.gender[0].id);
    });
  }

  public showPost(): void {
    this.servipostService.showPost().subscribe(
      (data: IformHeroes[]) => {
        this.superFakeList = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  ngOnInit(): void {
    this.createSuperHeroeForm();
    this.showPost();
  }

  public onSubmit(): void {

    this.submitted = true;

    if (this.formHeroes.valid) {
      const responseFormPost: IformHeroes = {
        namesuperheroe: this.formHeroes.get('namesuperheroe').value,
        fullname: this.formHeroes.get('fullname').value,
        gender: this.formHeroes.get('gender').value,
        origin: this.formHeroes.get('origin').value,
        intelligence: this.formHeroes.get('intelligence').value,
        strength: this.formHeroes.get('strength').value,
        speed: this.formHeroes.get('speed').value,
        durability: this.formHeroes.get('durability').value,
        power: this.formHeroes.get('power').value,
        combat: this.formHeroes.get('combat').value,
        image: this.formHeroes.get('image').value,
      };
      this.servipostService.postFormPost(responseFormPost).subscribe(
        (data: IformHeroes) => {
          console.log(data);
        },
        (err) => {
          console.error(err.message);
        }
      );
      this.formHeroes.reset();
      this.submitted = false;
    }
  }
  // tslint:disable-next-line: typedef
  getGenders(){
    return [
      { id: 'Female', name: 'Mujer' },
      { id: 'Male', name: 'Hombre' },
      { id: 'Unknown', name: 'Desconocido' },
    ];
  }
}
