import { Component, Input, OnInit } from '@angular/core';
import { IformHeroes } from '../../../../models/Iform';
import { ServipostService } from '../../../../services/servipost.service';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

  @Input() superHeroeFake: IformHeroes[] | any;

  constructor(private servipostService: ServipostService) { }

  ngOnInit(): void {
  }
}
