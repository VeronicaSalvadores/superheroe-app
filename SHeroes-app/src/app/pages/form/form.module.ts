import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form-component/form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PostFormComponent } from './form-component/post-form/post-form.component';


@NgModule({
  declarations: [FormComponent, PostFormComponent],
  imports: [
    CommonModule,
    FormRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class FormModule { }
