import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  items: Array<any> = [];

  constructor() {
    this.items = [
      { img: '/assets/img/gallery/imagen1.webp' },
      { img: '/assets/img/gallery/imagen2.png' },
      { img: '/assets/img/gallery/imagen3.jpeg' },
      { img: '/assets/img/gallery/imagen4.jpg' },
      { img: '/assets/img/gallery/imagen5.jpg' },
      { img: '/assets/img/gallery/imagen1.webp' },
      { img: '/assets/img/gallery/imagen2.png' },
      { img: '/assets/img/gallery/imagen3.jpeg' },
      { img: '/assets/img/gallery/imagen4.jpg' },
      { img: '/assets/img/gallery/imagen5.jpg' },
    ]
  }

  ngOnInit(): void {}

}
