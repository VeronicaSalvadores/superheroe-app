import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { GalleryComponent } from './home-component/gallery/gallery.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';


@NgModule({
  declarations: [HomeComponent, GalleryComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    Ng2CarouselamosModule,
  ]
})
export class HomeModule { }
