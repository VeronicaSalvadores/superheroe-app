import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { URL } from './../key/urls';
import { IformHeroes } from '../models/Iform';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class ServipostService {

  private posteo: IformHeroes[] | any;
  public backApiURL = URL.fakeapi;

  constructor(private http: HttpClient) {}

  public getFormPost(): IformHeroes[] {
    return this.posteo;
  }

  // tslint:disable-next-line: typedef
  public postFormPost(posteo: IformHeroes) {
    return this.http.post(this.backApiURL, posteo).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public showPost(): Observable<IformHeroes[]> {
    return this.http.get(this.backApiURL).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
