import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

import { IsuperHeroesDetails, IsuperHeroesGeneral } from '../models/Isuper-heroes';
import { URL } from './../key/urls';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
  })

export class SHeroesService {

  public superheroesURL = URL.apiKey;

  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  public getSuperHeroes(id: number): Observable<IsuperHeroesGeneral>{
    const Url = `${this.superheroesURL}${id}`;
    return this.http.get(Url).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          const formattedResults: IsuperHeroesGeneral = {
            id: response.id,
            name: response.name,
            image: response.image.url,
          };
          return formattedResults;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
  // tslint:disable-next-line: typedef
   public getSuperHeroesDetails(id: number): Observable<IsuperHeroesDetails>{
    const Url = `${this.superheroesURL}${id}`;
    return this.http.get(Url).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
/*             const formattedResults: IsuperHeroesDetails = {
            id: response.id,
            image: response.image,
            name: response.name,
            powerstats: response.Powerstats,
            biography: response.Biography,
            appearance: response.Appeareance,
          }; */
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
